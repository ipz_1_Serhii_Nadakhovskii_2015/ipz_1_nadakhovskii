import java.sql.*;
import java.net.*;
import java.io.*;
import java.util.concurrent.ThreadLocalRandom;

public class DBConnect extends Thread {

    protected Socket socket;
    private int client;
    private static boolean weather = true;

    public DBConnect(Socket s, int z){
        this.socket = s;
        this.client = z;
    }

    public void run(){
        Connection con;
        Statement st;
        InputStream sin;
        OutputStream sout;
        DataInputStream in = null;
        DataOutputStream out = null;
        int action_code, op = client;

        try {
            sin = socket.getInputStream();
            sout = socket.getOutputStream();
            in = new DataInputStream(sin);
            out = new DataOutputStream(sout);
        }catch (Exception ex){
            System.out.println("Input/Output connection failure.");
        }

            try {
                Class.forName("com.mysql.jdbc.Driver");
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/weather?characterEncoding=Cp1251", "root", "");
                st = con.createStatement();
            }catch (java.lang.ClassNotFoundException | java.sql.SQLException opp) {
                System.out.println("Data base error: " + opp);
                try {
                    out.writeInt(97);
                    out.flush();
                } catch (Exception e) {
                    System.out.println("Server - Client connection error: "+e);
                }
                return;

        }

        while (true) {
            try {
                action_code = in.readInt();
            } catch (Exception e) {
	            System.out.println("Client №"+op+" disconnected.");
                return;
            }

            switch (action_code) {
                case 1:
                    check(st, in, out, con, op);
                    break;
                case 2:
                    registration(st, in, out, con, op);
                    break;
                case 3:
                    history(st, in, out, op);
                    break;
                case 4:
                    date(st, in ,out, op);
                    break;
                case 5:
                    sign( st, in, out, op);
                    break;
                case 6:
                    range(st, in, out, op);
                    break;
	            case 9:
		            System.out.println("Client №"+op+" disconnected.");
		            return;
            }
        }

    }

    public void check(Statement st, DataInputStream in, DataOutputStream out, Connection con, int cl){
        try{
		    String query = "Select * from persons";
		    ResultSet rs = st.executeQuery(query);
		
		    boolean log = false;
		    String login_in = in.readUTF(), password_in = in.readUTF();
		    System.out.println("№" + cl + ": Got login: " + login_in + "  password: " + password_in);
		    System.out.println("№" + cl + ": Searching user...");
		    rs.beforeFirst();

            while (rs.next()){
                String login_out = rs.getString("login");
                String password_out = rs.getString("password");
                if (login_out.equals(login_in) && password_out.equals(password_in)){
                    System.out.println("№"+cl+": Found user!");
                    
                    if (weather){
                        weather(out,con,cl);
                    }
                    
                    log=true;
                    out.writeInt(1);
                    out.flush();
                    break;
                }
            }

            if (!log){
                System.out.println("№"+cl+": User not found.");
                out.writeInt(-1);
                out.flush();
            }


        }catch(Exception ex){
            System.out.println("№"+cl+": Server - Client connection error: "+ex);
	        try {
		        out.writeInt(97);
		        out.flush();
	        } catch (Exception e) {
		        System.out.println("№"+cl+": Server - Client connection error: "+e);
	        }
        }
    }

    public void registration(Statement st, DataInputStream in, DataOutputStream out, Connection con, int cl){
        try{
            String query = "Select * from persons";
            ResultSet rs = st.executeQuery(query);

            String first_name=in.readUTF(), second_name=in.readUTF(), phone_number = in.readUTF();
            String login = in.readUTF(), password = in.readUTF(), email = in.readUTF();
            System.out.println("№"+cl+": Got registration information.");

            rs.beforeFirst();

            while (rs.next()){
                String login_out = rs.getString("login");
                if (login_out.equals(login)){
                    System.out.println("№"+cl+": Login already used.");
                    out.writeInt(5);
                    out.flush();
                    return;
                }
            }

            int id = 1;
            rs.beforeFirst();
            while (rs.next()){
                id = rs.getInt("id");
            }
            id++;

            String sql = "INSERT INTO persons (id,first_name,second_name,phone_number,login,password,email)" +
                    "VALUES (?,?,?,?,?,?,?)";

            PreparedStatement pre_st = con.prepareStatement(sql);
            pre_st.setInt(1, id);
            pre_st.setString(2, first_name);
            pre_st.setString(3, second_name);
            pre_st.setString(4, phone_number);
            pre_st.setString(5, login);
            pre_st.setString(6, password);
            pre_st.setString(7, email);

            pre_st.execute();

            System.out.println("№"+cl+": Registration done");
            out.writeInt(1);
            out.flush();

        }catch(Exception ex){
            System.out.println("№"+cl+": Server - Client connection error: "+ex);
            try {
                out.writeInt(-1);
                out.flush();
            } catch (IOException e) {
                System.out.println("№"+cl+": Server - Client connection error: "+e);
            }
        }
    }

    public void date(Statement st, DataInputStream in, DataOutputStream out, int cl){
        try{
            String query = "Select * from city";
            ResultSet rs = st.executeQuery(query);

                String date = in.readUTF(), city = in.readUTF();
                System.out.println("№"+cl+": Weather date: "+date);
                System.out.println("№"+cl+": Searching...");

                rs.beforeFirst();
                while (rs.next()){
                    String date_out = rs.getString("date");
                    if (date_out.equals(date)){
                        out.writeInt(1);
    
                        int weather = rs.getInt(city);
    
                        switch (weather) {
                            case 1:
                                out.writeUTF("Сонячно");
                                break;
                            case 2:
                                out.writeUTF("Хмарно");
                                break;
                            case 3:
                                out.writeUTF("Дощ");
                                break;
                            case 4:
                                out.writeUTF("Град");
                                break;
                            case 5:
                                out.writeUTF("Сніг");
                                break;
                        }
                        
                        System.out.println("№"+cl+": Weather found.");
                        out.flush();
                        return;
                    }
                }

                    System.out.println("№"+cl+": Weather not found.");
                    out.writeInt(-2);
                    out.flush();

        }catch(Exception ex){
            System.out.println("№"+cl+": Server - Client connection error: "+ex);
            try {
                out.writeInt(-1);
                out.flush();
            } catch (IOException e) {
                System.out.println("№"+cl+": Server - Client connection error: "+e);
            }
        }
    }
    
    public void weather(DataOutputStream out, Connection con, int cl){
        try{
            for (int i=1; i<=61;i++) {
                int r1 = ThreadLocalRandom.current().nextInt(1, 6),
                        r2 = ThreadLocalRandom.current().nextInt(1, 6);
                String sql = "UPDATE `city` SET `Львів` = '" + r1 + "', `Київ` = '" + r2 + "' WHERE `city`.`id` = " + i + ";";
                PreparedStatement pre_st = con.prepareStatement(sql);
                pre_st.execute();
            }
        
            System.out.println("Random weather generated.");
            weather=false;
            
        }catch(Exception ex){
            System.out.println("№"+cl+": Server - Client connection error: "+ex);
            try {
                out.writeInt(-1);
                out.flush();
            } catch (IOException e) {
                System.out.println("№"+cl+": Server - Client connection error: "+e);
            }
        }
    }
    
    public void history(Statement st, DataInputStream in, DataOutputStream out, int cl){
        try{
            String query = "Select * from city", city = in.readUTF();
            ResultSet rs = st.executeQuery(query);
            
            System.out.println("№"+cl+": Searching weather history...");

            rs.beforeFirst();
            out.writeInt(61);
            while (rs.next()) {
                out.writeUTF(rs.getString("date"));
                int weather = rs.getInt(city);
    
                switch (weather) {
                    case 1:
                        out.writeUTF("Сонячно");
                        break;
                    case 2:
                        out.writeUTF("Хмарно");
                        break;
                    case 3:
                        out.writeUTF("Дощ");
                        break;
                    case 4:
                        out.writeUTF("Град");
                        break;
                    case 5:
                        out.writeUTF("Сніг");
                        break;
                }
            }
            out.flush();
            
            System.out.println("№"+cl+": Weather history found.");
            
        }catch(Exception ex){
            System.out.println("№"+cl+": Server - Client connection error: "+ex);
            try {
                out.writeInt(-1);
                out.flush();
            } catch (IOException e) {
                System.out.println("№"+cl+": Server - Client connection error: "+e);
            }
        }
    }
    
    public void sign(Statement st, DataInputStream in, DataOutputStream out, int cl){
        try{
            String query = "Select * from city", city = in.readUTF();
            ResultSet rs = st.executeQuery(query);
            int number = 0, sign = in.readInt();
            
            System.out.println("№"+cl+": Got weather");
            System.out.println("№"+cl+": Searching...");
    
            rs.beforeFirst();
            while (rs.next()){
                if (rs.getInt(city) == sign){
                    number++;
                }
            }
            
            out.writeInt(number);
            
            rs.beforeFirst();
            while (rs.next()){
                if (rs.getInt(city) == sign){
                    out.writeUTF(rs.getString("date"));
                    out.flush();
                }
            }
            
        }catch(Exception ex){
            System.out.println("№"+cl+": Server - Client connection error: "+ex);
            try {
                out.writeInt(-1);
                out.flush();
            } catch (IOException e) {
                System.out.println("№"+cl+": Server - Client connection error: "+e);
            }
        }
    }
    
    public void range(Statement st,DataInputStream in, DataOutputStream out, int cl){
        try{
            String query = "Select * from city";
            ResultSet rs = st.executeQuery(query);
            
            String date_1=in.readUTF(), date_2=in.readUTF(), city = in.readUTF();
            int number=0;
            boolean r=false;
            
            System.out.println("№"+cl+": Got range: "+date_1+"-"+date_2);
    
            rs.beforeFirst();
            while (rs.next()){
                if (rs.getString("date").equals(date_1)){
                    r=true;
                }
                if (r){
                    number++;
                }
                if (rs.getString("date").equals(date_2)){
                    break;
                }
            }
            
            r=false;
            
            if (number==0){
                out.writeInt(-2);
                return;
            }
            
            rs.beforeFirst();
            out.writeInt(number);
            while (rs.next()) {
                if (rs.getString("date").equals(date_1)){
                    r=true;
                }
                if (r) {
                    out.writeUTF(rs.getString("date"));
                    int weather = rs.getInt(city);
    
                    switch (weather) {
                        case 1:
                            out.writeUTF("Сонячно");
                            break;
                        case 2:
                            out.writeUTF("Хмарно");
                            break;
                        case 3:
                            out.writeUTF("Дощ");
                            break;
                        case 4:
                            out.writeUTF("Град");
                            break;
                        case 5:
                            out.writeUTF("Сніг");
                            break;
                    }
                }
                if (rs.getString("date").equals(date_2)){
                    break;
                }
                
            }
            out.flush();
            
            System.out.println("№"+cl+":  Executed.");
            
        }catch(Exception ex){
            System.out.println("№"+cl+": Server - Client connection error: "+ex);
            try {
                out.writeInt(-1);
                out.flush();
            } catch (IOException e) {
                System.out.println("№"+cl+": Server - Client connection error: "+e);
            }
        }
    }
}
