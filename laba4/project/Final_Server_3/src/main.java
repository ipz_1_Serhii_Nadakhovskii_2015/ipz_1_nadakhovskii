import java.net.*;

public class main {
    
    public static void main(String[] args) {
        ServerSocket ss = null;
        Socket socket = null;
        int clients=0;

        System.out.println("Server started");
        System.out.println("Waiting for connections...");
        try{
            ss = new ServerSocket(1237);
        }catch (Exception e) {
            System.out.println("Port 1237 already in use.");
        }

        while (true){
            try{
                socket = ss.accept();
                clients++;
            }catch (Exception e) {
                System.out.println("Client connection failure.");
            }
            System.out.println("Client №"+clients+" connected.");
            new DBConnect(socket, clients).start();
        }

    }

}
