-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Час створення: Гру 12 2017 р., 19:25
-- Версія сервера: 10.1.26-MariaDB
-- Версія PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `weather`
--

-- --------------------------------------------------------

--
-- Структура таблиці `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `date` varchar(15) COLLATE cp1251_ukrainian_ci NOT NULL,
  `Львів` int(11) NOT NULL,
  `Київ` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci;

--
-- Дамп даних таблиці `city`
--

INSERT INTO `city` (`id`, `date`, `Львів`, `Київ`) VALUES
(1, '01.10.2017', 2, 5),
(2, '02.10.2017', 3, 2),
(3, '03.10.2017', 3, 2),
(4, '04.10.2017', 1, 4),
(5, '05.10.2017', 5, 3),
(6, '06.10.2017', 5, 4),
(7, '07.10.2017', 1, 4),
(8, '08.10.2017', 5, 2),
(9, '09.10.2017', 1, 1),
(10, '10.10.2017', 2, 4),
(11, '11.10.2017', 4, 2),
(12, '12.10.2017', 4, 3),
(13, '13.10.2017', 4, 2),
(14, '14.10.2017', 3, 1),
(15, '15.10.2017', 4, 1),
(16, '16.10.2017', 1, 5),
(17, '17.10.2017', 5, 3),
(18, '18.10.2017', 4, 5),
(19, '19.10.2017', 1, 4),
(20, '20.10.2017', 5, 2),
(21, '21.10.2017', 3, 2),
(22, '22.10.2017', 1, 1),
(23, '23.10.2017', 5, 4),
(24, '24.10.2017', 4, 2),
(25, '25.10.2017', 5, 4),
(26, '26.10.2017', 1, 4),
(27, '27.10.2017', 1, 3),
(28, '28.10.2017', 2, 5),
(29, '29.10.2017', 4, 5),
(30, '30.10.2017', 2, 2),
(31, '31.10.2017', 3, 3),
(32, '01.11.2017', 2, 3),
(33, '02.11.2017', 1, 4),
(34, '03.11.2017', 4, 2),
(35, '04.11.2017', 3, 1),
(36, '05.11.2017', 1, 2),
(37, '06.11.2017', 2, 2),
(38, '07.11.2017', 1, 4),
(39, '08.11.2017', 2, 3),
(40, '09.11.2017', 4, 1),
(41, '10.11.2017', 4, 4),
(42, '11.11.2017', 1, 5),
(43, '12.11.2017', 1, 5),
(44, '13.11.2017', 3, 1),
(45, '14.11.2017', 4, 5),
(46, '15.11.2017', 4, 5),
(47, '16.11.2017', 4, 1),
(48, '17.11.2017', 3, 1),
(49, '18.11.2017', 2, 5),
(50, '19.11.2017', 2, 1),
(51, '20.11.2017', 3, 5),
(52, '21.11.2017', 5, 3),
(53, '22.11.2017', 2, 5),
(54, '23.11.2017', 2, 4),
(55, '24.11.2017', 3, 5),
(56, '25.11.2017', 3, 4),
(57, '26.11.2017', 3, 4),
(58, '27.11.2017', 1, 5),
(59, '28.11.2017', 1, 3),
(60, '29.11.2017', 2, 5),
(61, '30.11.2017', 5, 5);

-- --------------------------------------------------------

--
-- Структура таблиці `persons`
--

CREATE TABLE `persons` (
  `id` int(11) NOT NULL,
  `first_name` varchar(30) COLLATE cp1251_ukrainian_ci NOT NULL,
  `second_name` varchar(30) COLLATE cp1251_ukrainian_ci NOT NULL,
  `phone_number` varchar(15) COLLATE cp1251_ukrainian_ci NOT NULL,
  `login` varchar(30) COLLATE cp1251_ukrainian_ci NOT NULL,
  `password` varchar(30) COLLATE cp1251_ukrainian_ci NOT NULL,
  `email` varchar(30) COLLATE cp1251_ukrainian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci;

--
-- Дамп даних таблиці `persons`
--

INSERT INTO `persons` (`id`, `first_name`, `second_name`, `phone_number`, `login`, `password`, `email`) VALUES
(1, 'Олег', 'Олегов', '0675522333', 'Oleg', 'oleg', 'Oleg@gmail.com'),
(2, 'Петро', 'Петро', '0975544111', 'Petro', 'petro', 'Petro@yahoo.com'),
(3, 'Андрій', 'Андрійов', '0675544333', 'Andrew', 'andrew', 'andrew@gmail.com'),
(4, 'Роман', 'Романов', '0684499111', 'Roman', 'roman', 'Roman@gmail.com'),
(5, 'Олексій', 'Олексіїв', '0554477111', 'Lesha', 'lesha', 'Lesha@gmail.com');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
